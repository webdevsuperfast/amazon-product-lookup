(function($){
    $('#formInformation').submit(function(event){
        var data = $(this).serialize();
        $('.additional-information').html('<div class="col-sm-12"><img src="images/loading.gif" alt="loading" /></div>');
        event.preventDefault();
        $.post('includes/add.php', data, function( data ){
            $('.additional-information').html('Hello World');
            // console.log('Hello World');
        });
    });

    $('#asinForm').submit(function(event){
        var data = $(this).serialize();
        $('.content').html('<div class="col-sm-12"><img src="images/loading.gif" alt="loading" /></div>');
        event.preventDefault();
        $.post('includes/lookup.php', data, function( data ){
            $('.content').html(data);
        });
    });
    $(document).ready(function(){
        $('#asin').typeahead({
            hint: true,
            highlight: true,
            minLength: 1    
        }, {
            name: 'asin',
            /* source: function(query, process){
                $.ajax({
                    url: 'includes/asin.php',
                    type: 'POST',
                    data: 'asin=' + query,
                    dataType: 'JSON',
                    async: false,
                    success: function(data) {
                        process(data);
                        // console.log(data);
                        // return data;
                    }
                });
            } */
            source: function(query, process) {
                $.ajaxSetup({async: false});
                $.post('includes/asin.php', {asin: query}, function(data){
                    process(data);
                    // console.log(data);
                }, 'json');
                $.ajaxSetup({async: true});
            }
        });

        $('#category').typeahead({
            hint: true,
            highlight: true,
            minLength: 1    
        }, {
            name: 'category',
            /* source: function(query, process){
                $.ajax({
                    url: 'includes/category.php',
                    type: 'POST',
                    data: 'category=' + query,
                    dataType: 'JSON',
                    async: false,
                    success: function(data) {
                        process(data);
                        // console.log(data);
                        // return data;
                    }
                });
            } */
            source: function(query, process) {
                $.ajaxSetup({async: false});
                $.post('includes/category.php', {category: query}, function(data){
                    process(data);
                    // console.log(data);
                }, 'json');
                $.ajaxSetup({async: true});
            }
        });
    });
})(jQuery);
