<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);

require_once "../vendor/autoload.php";
include "config.php";
include 'xml.php';

$connection = new PDO( 'mysql:host=localhost;dbname=' . $dbname, $dbuser, $dbpass );
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

use ApaiIO\ApaiIO;
use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\Lookup;

$conf = new GenericConfiguration();
$client = new \GuzzleHttp\Client();
$request = new \ApaiIO\Request\GuzzleRequest($client);

try {
    $conf
        ->setCountry('com')
        ->setAccessKey('AKIAIGSD5HKIGUD4OR2A')
        ->setSecretKey('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')
        ->setAssociateTag('woodingtradin-20')
        ->setRequest($request)
        ->setResponseTransformer(new \Includes\RecommendWP\ItemSearchXmlToItems());
        // ->setResponseTransformer(new \ApaiIO\ResponseTransformer\XmlToSimpleXmlObject());
} catch (\Exception $e) {
    echo $e->getMessage();
}

if ( isset( $_POST['asin'] ) && !empty( $_POST['asin'] ) ) {
    $asin = $_POST['asin'];
    $category = $_POST['category'];

    try {
        $sql = "SELECT * FROM `reviews`"
        . " WHERE ( `asin` LIKE '%".$asin."%' AND `category`"
        . " LIKE '%".$category."%' )";
        $statement = $connection->query( $sql );
        $results = $statement->fetchAll( PDO::FETCH_ASSOC );
    } catch( PDOException $e ) {
        echo $e->getMessage();
    }

    if ( $results[0]['asin'] == $asin && $results[0]['category'] == $category ) {
        $apaiIO = new ApaiIO($conf);
        $lookup = new Lookup();
        $lookup->setItemId($asin);
        $lookup->setResponseGroup(array('Medium'));

        $response = $apaiIO->runOperation( $lookup ); 
        $result = $response['Items']['Item']; ?>
    <?php } else {
        try {
            $sql = "INSERT INTO `reviews`"
            . " (asin, category, added)"
            . " VALUES (:asin, :category, NOW())";
            $statement = $connection->prepare( $sql );
            $statement->execute( array( ':asin' => $asin, ':category' => $category ) );
        } catch( PDOException $e ) {
            echo $e->getMessage();
        }
        $apaiIO = new ApaiIO($conf);
        $lookup = new Lookup();
        $lookup->setItemId($asin);
        $lookup->setResponseGroup(array('Medium'));

        $response = $apaiIO->runOperation( $lookup ); 
        $result = $response['Items']['Item']; ?>
    <?php } ?>
    <div class="col-sm-6">
        <div class="ratings">
            <?php
            $r_5 = isset( $_POST['rate_5'] ) ? $_POST['rate_5'] : $results[0]['five_star_reviews'];
            $r_4 = isset( $_POST['rate_4'] ) ? $_POST['rate_4'] : $results[0]['four_star_reviews'];
            $r_3 = isset( $_POST['rate_3'] ) ? $_POST['rate_3'] : $results[0]['three_star_reviews'];
            $r_2 = isset( $_POST['rate_2'] ) ? $_POST['rate_2'] : $results[0]['two_star_reviews'];
            $r_1 = isset( $_POST['rate_1'] ) ? $_POST['rate_1'] : $results[0]['one_star_reviews'];
            $r_pros = isset( $_POST['pros'] ) ? $_POST['pros'] : $results[0]['pros'];
            $r_cons = isset( $_POST['cons'] ) ? $_POST['cons'] : $results[0]['cons'];
            $r_content = isset( $_POST['content'] ) ? $_POST['content'] : $results[0]['content'];
            $r_reviews = isset( $_POST['customer_reviews'] ) ? $_POST['customer_reviews'] : $results[0]['customer_reviews']; 
            $r_category = isset( $_POST['category'] ) ? $_POST['category'] : $results[0]['category']; ?>
        </div>
        <div class="additional-information">
            <form method="post" id="formInformation" action="">
                <div class="form-group">
                    <input type="number" class="form-control" id="customer_reviews" name="customer_reviews" value="<?php echo $r_reviews; ?>" placeholder="Customer Reviews">
                </div>
                <div class="form-group">
                    <input type="number" class="form-control" id="rate_5" name="rate_5" value="<?php echo $r_5; ?>" placeholder="5 Ratings">
                </div>
                <div class="form-group">
                    <input type="number" class="form-control" id="rate_4" name="rate_4" value="<?php echo $r_4; ?>" placeholder="4 Ratings">
                </div>
                <div class="form-group">
                    <input type="number" class="form-control" id="rate_3" name="rate_3" value="<?php echo $r_3; ?>" placeholder="3 Ratings">
                </div>
                <div class="form-group">
                    <input type="number" class="form-control" id="rate_2" name="rate_2" value="<?php echo $r_2; ?>" placeholder="2 Ratings">
                </div>
                <div class="form-group">
                    <input type="number" class="form-control" id="rate_1" name="rate_1" value="<?php echo $r_1; ?>" placeholder="1 Ratings">
                </div>
                <div class="form-group">
                    <textarea id="pros" cols="30" rows="3" class="form-control" name="pros" placeholder="Pros, one per line"><?php echo $r_pros; ?></textarea>
                    <p class="help-block" id="points_counter"></p>
                </div>
                <div class="form-group">
                    <textarea id="cons" cols="30" rows="3" class="form-control" name="cons" placeholder="Cons, one per line"><?php echo $r_cons; ?></textarea>
                    <p class="help-block" id="points_counter"></p>
                </div>
                <div class="form-group">
                    <textarea id="content" cols="30" rows="3" class="form-control" name="content" placeholder="Content"><?php echo $r_content; ?></textarea>
                    <p class="help-block" id="content_counter"></p>
                </div>
                <div class="form-group">
                    <input type="number" class="form-control" id="cat" name="cat" value="<?php echo $r_category; ?>" placeholder="Category">
                </div>
                <input type="hidden" name="asinfile" value="<?php echo $asin; ?>">
                <input type="submit" class="btn btn-success" id="save" name="save" value="Search">
            </form>
        </div>
    </div>
    <div class="col-sm-6">
        <h4 class="title"><?php echo $result['ItemAttributes']['Title']; ?><span class="badge pull-right"><?php echo $result['ItemAttributes']['ProductGroup']; ?></span></h4>
        <?php
        $features = $result['ItemAttributes']['Feature'];
        $reviews = $result['EditorialReviews']['EditorialReview']['Content'];
        $price = $result['ItemAttributes']['ListPrice']['FormattedPrice'];
        $usedprice = $result['OfferSummary']['LowestUsedPrice']['FormattedPrice'];
        $newprice = $result['OfferSummary']['LowestNewPrice']['FormattedPrice'];
        $largeimage = $result['LargeImage']['URL'];
        ?>
        <?php if ( !empty( $price ) ) { ?>      
        <p>Price: <span class="label label-default"><?php echo $price; ?></span></p>
        <?php } ?>
        <?php if ( !empty( $newprice ) ) { ?>
        <p>Lowest New Price: <span class="label label-success"><?php echo $newprice; ?></span></p>
        <?php } ?>
        <?php if ( !empty( $usedprice ) ) { ?>
        <p>Lowest Used Price: <span class="label label-warning"><?php echo $usedprice; ?></span></p>
        <?php } ?>

        <?php if ( !empty( $largeimage ) ) { ?>
        <div class="text-center">
            <img src="<?php echo $result['LargeImage']['URL']; ?>" alt="<?php echo $result['ItemAttributes']['Title']; ?>" class="img-responsive img-product">
        </div>
        <?php } ?>

        <?php if ( !empty( $features ) ) { ?>
        <div class="panel panel-info">
            <div class="panel-heading">Product Features</div>
            <div class="panel-body">
                <ul>
                <?php foreach ( $features as $feature ) {
                    echo '<li>' . $feature . '</li>';
                } ?>
                </ul>
            </div>
        </div>
        <?php } ?>
        
        <?php if ( !empty( $reviews ) ) { ?>
        <div class="panel panel-info">
            <div class="panel-heading">Editorial Review</div>
            <div class="panel-body"><p><?php  echo $result['EditorialReviews']['EditorialReview']['Content']; ?></p></div>
        </div>
        <?php } ?>

        <a target="_blank" href="<?php echo $result['DetailPageURL']; ?>" class="btn btn-primary btn-lg">View it on Amazon</a>
    </div>
<?php } else { ?>
    <div class="col-sm-12">
    <div class="alert alert-warning"><strong>ASIN</strong> needs to be valid.</div>
    </div>
<?php }