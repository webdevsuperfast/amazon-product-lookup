<?php
include "config.php";

$connection = new PDO( 'mysql:host=localhost;dbname=' . $dbname, $dbuser, $dbpass );
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if( isset( $_POST['category'] ) ) {
    $category = $_POST['category'];
    try {
        $sql = "SELECT DISTINCT `category` FROM `reviews` WHERE `category` LIKE '%".$category."%'";
        $statement = $connection->query( $sql );
        $results = $statement->fetchAll( PDO::FETCH_ASSOC );
    } catch( PDOException $e ) {
        echo $e->getMessage();
    }

    $array = array();

    foreach( $results as $result ) {
        $array[] = $result['category'];
    }

    echo json_encode( $array );
}