<?php
include "config.php";

$connection = new PDO( 'mysql:host=localhost;dbname=' . $dbname, $dbuser, $dbpass );
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// if ( isset( $_POST['asinfile'] ) ) {
    $asin = 'B01ENGT2IM'; // $_POST['asinfile'];
    $category = 'nose job'; // $_POST['cat'];
    $rate_5 = isset( $_POST['rate_5'] ) ? intval( $_POST['rate_5'] ) : '';
    $rate_4 = isset( $_POST['rate_4'] ) ? intval( $_POST['rate_4'] ) : '';
    $rate_3 = isset( $_POST['rate_3'] ) ? intval( $_POST['rate_3'] ) : '';
    $rate_2 = isset( $_POST['rate_2'] ) ? intval( $_POST['rate_2'] ) : '';
    $rate_1 = isset( $_POST['rate_1'] ) ? intval( $_POST['rate_1'] ) : '';
    $pros = isset( $_POST['pros'] ) ? $_POST['pros'] : '';
    $cons = isset( $_POST['cons'] ) ? $_POST['cons'] : '';
    $content = isset( $_POST['content'] ) ? $_POST['content'] : '';
    $customer_reviews = isset( $_POST['customer_reviews'] ) ? $_POST['customer_reviews'] : '';
    
    $sql = "INSERT INTO `reviews` (asin, customer_reviews, five_star_reviews, "
        . "four_star_reviews, three_star_reviews, two_star_reviews, one_star_reviews, pros, cons, content, category, added) VALUES (:asin, :customer_reviews, :five_star_reviews, :four_star_reviews, :three_star_reviews, :two_star_reviews, :one_star_reviews, :pros, :cons, :content, :category, NOW()) ";

    $statement = $connection->prepare( $sql );
    $statement->execute( array( ':asin' => $asin, ':customer_reviews' => $customer_reviews, ':five_star_reviews' => $rate_5, ':four_star_reviews' => $rate_4, ':three_star_reviews' => $rate_3, ':two_star_reviews' => $rate_2, ':one_star_reviews' => $rate_1, ':pros' => $pros, ':cons' => $cons, ':content' => $content, ':category' => $category ) );
// }