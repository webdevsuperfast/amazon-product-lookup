<?php 
namespace Includes\RecommendWP;

use ApaiIO\ResponseTransformer\ResponseTransformerInterface;

class ItemSearchXmlToItems implements ResponseTransformerInterface
{
    public function transform($response)
    {
        $xml = simplexml_load_string( $response );
        $array = json_decode( json_encode( $xml ), true );
        
        return $array;
    }
}