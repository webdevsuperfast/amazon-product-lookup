</div>
<footer class="site-footer">
    <div class="container">
        <p class="copyright">&copy; Copyright 2017. <?php echo $sitetitle; ?>. All rights reserved.</p>
    </div>
</footer>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/Countable.min.js"></script>
<script src="assets/js/typeahead.jquery.min.js"></script>
<script src="assets/js/app.js"></script>
</body>
</html>