<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $sitetitle; ?></title>
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="assets/css/app.css">
</head>
<body>
<header class="navbar navbar-inverse navbar-static-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo $siteurl; ?>"><?php echo $sitetitle; ?></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<form class="navbar-form navbar-right" method="post" id="asinForm" action="">
				<div class="form-group">
					<input type="text" placeholder="ASIN" name="asin" class="form-control" id="asin">
				</div>
				<div class="form-group">
					<input type="text" placeholder="Category" name="category" class="form-control" id="category">
				</div>
				<input type="submit" class="btn btn-success" id="submit" name="submit" value="Search">
			</form>
		</div><!--/.navbar-collapse -->
	</div>
</header>
<div class="site-inner">