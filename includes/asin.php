<?php
include "config.php";

$connection = new PDO( 'mysql:host=localhost;dbname=' . $dbname, $dbuser, $dbpass );
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if( isset( $_POST['asin'] ) ) {
    $asin = $_POST['asin'];
    try {
        $sql = "SELECT DISTINCT `asin` FROM `reviews` WHERE `asin` LIKE '%".$asin."%'";
        $statement = $connection->query( $sql );
        $results = $statement->fetchAll( PDO::FETCH_ASSOC );
    } catch( PDOException $e ) {
        echo $e->getMessage();
    }

    $array = array();

    foreach( $results as $result ) {
        $array[] = $result['asin'];
    }

    echo json_encode( $array );
}