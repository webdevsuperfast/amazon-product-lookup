<?php
error_reporting(E_ALL);
ini_set('display_errors', 1); 
include 'includes/config.php';

$db = new PDO( 'mysql:host=localhost;dbname=' . $dbname, $dbuser, $dbpass );

try {
    $sql = "CREATE TABLE IF NOT EXISTS `reviews`"
    . " (`id` int(11) NOT NULL AUTO_INCREMENT,"
    . " `asin` varchar(30) NOT NULL,"
    . " `five_star_reviews` int(11) NOT NULL,"
    . " `four_star_reviews` int(11) NOT NULL,"
    . " `three_star_reviews` int(11) NOT NULL,"
    . " `two_star_reviews` int(11) NOT NULL,"
    . " `one_star_reviews` int(11) NOT NULL,"
    . " `pros` text NOT NULL,"
    . " `cons` text NOT NULL,"
    . " `content` text NOT NULL,"
    . " `category` text NOT NULL,"
    . " `added` datetime NOT NULL,"
    . " PRIMARY KEY (`id`))"
    . " ENGINE=InnoDB";

    $db->exec( $sql );
    
} catch( PDOException $e ) {
    echo $e->getMessage();
}

// $asin = isset( $_POST['asin'] ) ? $_POST['asin'] : '';
try {
    $sql = "SELECT * FROM `reviews` ORDER BY `added` DESC";
    $statement = $db->query( $sql );
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
} catch( Exception $e ) {
    echo $e->getMessage();
}

include 'includes/header.php'; ?>

<div class="container">
    <div class="row content">
        <div class="col-sm-12">
            <?php if ( !empty( $results ) ) { ?>
                <?php $num = 1; ?>
                <div class="responsive-table">
                    <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>ASIN</th>
                            <th>Category</th>
                            <th>View</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php foreach( $results as $result ) { ?>
                    <tr>
                        <td><?php echo $num; ?></td>
                        <td><?php echo $result['asin']; ?></td>
                        <td><?php echo $result['category']; ?></td>
                        <td>Test</td>
                    </tr>
                    <?php $num++; ?>
                <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>No.</td>
                            <td>ASIN</td>
                            <td>Category</td>
                            <td>View</td>
                        </tr>
                    </tfoot>
                    </table>
                </div>
            <?php } else {
                echo '<div class="alert alert-warning"><strong>Oops!</strong> no recent searches found.</div>';
            } ?>
        </div>
    </div>
</div>

<?php include 'includes/footer.php'; ?>